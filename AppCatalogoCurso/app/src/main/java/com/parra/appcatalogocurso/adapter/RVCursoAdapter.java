package com.parra.appcatalogocurso.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.parra.appcatalogocurso.R;
import com.parra.appcatalogocurso.model.Curso;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RVCursoAdapter extends RecyclerView.Adapter<RVCursoAdapter.CursoViewHolder> {
    public static final String TAG = RVCursoAdapter.class.getSimpleName();

    private ArrayList<Curso> lsCursos;
    private Activity activity;
    private int resurce;

    public RVCursoAdapter(ArrayList<Curso> lsCursos, Activity activity, int resurce) {
        this.lsCursos = lsCursos;
        this.activity = activity;
        this.resurce = resurce;

        }

    @NonNull
    @Override
    public CursoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(resurce,parent,false);
        CursoViewHolder cursoViewHolder = new CursoViewHolder(view);

        return cursoViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CursoViewHolder holder, int position) {
    Curso curso = lsCursos.get(position);

         Picasso.get().load(curso.getUrlFoto()).into(holder.imgUrlFotoCard);
        //Picasso.get().load(curso.getUrlFoto()).into(holder.imgUrlFotoCard);
        holder.txtidCard.setText(String.valueOf(curso.getIdCur()));
        holder.txtNombreCard.setText(curso.getNombreCur());
        holder.txtCreditosCard.setText(String.valueOf(curso.getCreditosCur()));
      holder.txtPrecioCard.setText(String.valueOf(curso.getPrecio()));
        holder.txtHorarioCard.setText(String.valueOf(curso.getHorarioCur()));
        holder.txtTipoCurCard.setText(String.valueOf(curso.getIdtipoCur()));


    }

    @Override
    public int getItemCount() {
        return lsCursos.size();
    }
    //Clase embebida

    public class CursoViewHolder extends RecyclerView.ViewHolder{
        CardView cvCurso;
        ImageView imgUrlFotoCard;
        TextView txtidCard,txtNombreCard,txtCreditosCard,txtPrecioCard,txtHorarioCard,txtTipoCurCard;
        public CursoViewHolder(@NonNull View itemView) {
            super(itemView);

            cvCurso =itemView.findViewById(R.id.cvCurso);
            txtidCard =itemView.findViewById(R.id.txtIdCard);
            txtNombreCard =itemView.findViewById(R.id.txtNombreCard);
            txtCreditosCard =itemView.findViewById(R.id.txtCreditosCard);
            txtPrecioCard =itemView.findViewById(R.id.txtPrecioCard);
            txtHorarioCard =itemView.findViewById(R.id.txtHorarioCard);
            txtTipoCurCard =itemView.findViewById(R.id.txtTipoCurCard);
            imgUrlFotoCard =itemView.findViewById(R.id.imgUrlFotoCard);
        }
    }
}
