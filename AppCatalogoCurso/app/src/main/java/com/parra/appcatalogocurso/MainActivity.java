package com.parra.appcatalogocurso;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.parra.appcatalogocurso.adapter.RVCursoAdapter;
import com.parra.appcatalogocurso.interfaces.IJsonCursoApi;
import com.parra.appcatalogocurso.model.Curso;
import com.parra.appcatalogocurso.utils.Constantes;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    CardView cvCurso;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        initObjects();

    }

    /**
     Metodo que se conecta al servicio WEB mediante retrofit
     @return
     * */
    public void  onListarCurso(View view) {
        final ArrayList<Curso> lsNewCurso = new ArrayList<>();
       Log.d(TAG,">>>metodo onListarCurso()");
        Retrofit retrofit= new Retrofit.Builder()
                .baseUrl(Constantes.URL_CATALOGO_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        IJsonCursoApi jsonCursoApi = retrofit.create(IJsonCursoApi.class);

        Call<List<Curso>> call = jsonCursoApi.listarCurso();

        call.enqueue(new Callback<List<Curso>>() {
            @Override
            public void onResponse(Call<List<Curso>> call, Response<List<Curso>> response) {
                Log.d(TAG,">>>Metodo onResponse");


                if(!response.isSuccessful()){
                    //para saber cual es el codigo de error
                    String mensaje = "Error:"+response.code();
                    Toast.makeText(MainActivity.this,mensaje, Toast.LENGTH_SHORT).show();
                }else{
                    List<Curso> lsCurso= response.body();
                    Curso objCurso = null;
                    for(Curso curso:lsCurso){

                    objCurso.setIdCur(curso.getIdCur());
                    objCurso.setNombreCur(curso.getNombreCur());
                    objCurso.setUrlFoto(curso.getUrlFoto());
                    objCurso.setCreditosCur(curso.getCreditosCur());
                    objCurso.setPrecio(curso.getPrecio());
                    objCurso.setHorarioCur(curso.getHorarioCur());
                    objCurso.setIdtipoCur(curso.getIdtipoCur());
                    lsNewCurso.add(objCurso);
                    }

                }
            }

            @Override
            public void onFailure(Call<List<Curso>> call, Throwable t) {
                String mensajeError = "ERROR: "+t.getMessage();
                Log.d(TAG,">>>Metodo onFailure()");
                Toast.makeText(MainActivity.this,mensajeError,Toast.LENGTH_SHORT).show();
            }
        });
        //01Manager
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);

        //02Manager
        RVCursoAdapter rvCursoAdapter = new RVCursoAdapter(lsNewCurso, this,R.layout.cardview_curso);

        //03 recycler
        RecyclerView rvCurso = findViewById(R.id.rvCurso);
        rvCurso.setLayoutManager(linearLayoutManager);
        rvCurso.setAdapter(rvCursoAdapter);
        /**  return
         Prueba local para cargar datos iniciales a los cardview
         **/
    }



    private ArrayList<Curso> listarCrusosPrueba(){
        ArrayList<Curso> lsCursos = new ArrayList<Curso>();

        lsCursos.add(new Curso(
                1,
                "Matematicas",
                5,
                20.5,
                "Solo Lunes",
                "https://imagenes.universia.net/gc/net/images/educacion/8/8-/8-c/8-cursos-gratis-sobre-matematicas.jpg",
                2) );

        lsCursos.add(new Curso(
                2,
                "Matematicas",
                5,
                20.5,
                "Solo Lunes",
                "https://imagenes.universia.net/gc/net/images/educacion/8/8-/8-c/8-cursos-gratis-sobre-matematicas.jpg",
                2) );
        return lsCursos;
    }



    private void initObjects() {
            Log.d(TAG,">>>Metodo initObjects()");
            cvCurso = findViewById(R.id.cvCurso);


    }
}
