package com.parra.appcatalogocurso.interfaces;

import com.parra.appcatalogocurso.model.Curso;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface IJsonCursoApi {
@GET("/api/Curs")
    Call<List<Curso>> listarCurso();


}
