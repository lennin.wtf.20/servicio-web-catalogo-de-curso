package com.parra.appcatalogocurso.model;

public class Curso {
    private int idCur;
    private String nombreCur;
    private int creditosCur;
    private Double precio;
    private String horarioCur;
    private String urlFoto;
    private int idtipoCur;

    public Curso() {
    }

    public Curso(int idCur, String nombreCur, int creditosCur, Double precio, String horarioCur, String urlFoto, int idtipoCur) {
        this.idCur = idCur;
        this.nombreCur = nombreCur;
        this.creditosCur = creditosCur;
        this.precio = precio;
        this.horarioCur = horarioCur;
        this.urlFoto = urlFoto;
        this.idtipoCur = idtipoCur;
    }

    public int getIdCur() {
        return idCur;
    }

    public void setIdCur(int idCur) {
        this.idCur = idCur;
    }

    public String getNombreCur() {
        return nombreCur;
    }

    public void setNombreCur(String nombreCur) {
        this.nombreCur = nombreCur;
    }

    public int getCreditosCur() {
        return creditosCur;
    }

    public void setCreditosCur(int creditosCur) {
        this.creditosCur = creditosCur;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getHorarioCur() {
        return horarioCur;
    }

    public void setHorarioCur(String horarioCur) {
        this.horarioCur = horarioCur;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public int getIdtipoCur() {
        return idtipoCur;
    }

    public void setIdtipoCur(int idtipoCur) {
        this.idtipoCur = idtipoCur;
    }
}
