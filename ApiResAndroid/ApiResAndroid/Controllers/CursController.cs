﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Datos.Models;

namespace ApiResAndroid.Controllers
{
    public class CursController : ApiController
    {
        // GET: api/Curs
        public IEnumerable<Curso> Get()
        {
            var lsCursos = new List<Curso>();
            using (db_androidEntities db = new db_androidEntities())
                lsCursos = db.Curso.ToList();
            return lsCursos;
        }

        // GET: api/Curs/5
        public Curso Get(int id)
        {
            var curso = new Curso();
            using (db_androidEntities db = new db_androidEntities())
            {
                //id de curso se compara con otro id
                var Curso = db.Curso.FirstOrDefault(x => x.idCur == id);
            }
            return curso;
        }


            // POST: api/Curs
            public void Post([FromBody]string value)
            {
                using (db_androidEntities db = new db_androidEntities()) {
                    var curso = new Curso();
                    curso.idCur = 0;
                    curso.nombreCur = "Mi nuevo curso";
                    curso.precioCur = 25;
                    curso.idTipoCur = 1;
                curso.urlFotoCur = "";
                curso.creditosCur = 2;
                db.SaveChanges();
                }

        }

            // PUT: api/Curs/5
            public void Put(int id, [FromBody]string value)
            {
            }

            // DELETE: api/Curs/5
            public void Delete(int id)
            {
            }
        }
    }

